# Team 26 UPS uHAT Project for EEE3088F
A UPS μHAT for the Raspberry Pi family of microcontrollers.

The UPS is compatible with single cell Li-Ion batteries.

## Documentation
All documentation for this project can be found [here](docs/)

## Sub-Modules
PSU & Battery Charger - FSTGAV002<br/>
Gas Gauge & Status LEDS - VLHLEF001<br/>
Analog Battery Status - OWSLEZ001
