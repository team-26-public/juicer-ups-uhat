# How to use the Juicer UPS uHAT
**Note:** The Juicer UPS uHAT provides a separate micro-USB power input. This input is for both the uHAT, as well as the Pi. Do NOT plug in the HAT and the Pi separately.

1. Shut down the Pi;
2. Unplug the Pi's micro-USB power cable;
3. Carefully fit the the uHAT's GPIO connector over the Pi's GPIO pins. Ensure that the orientation is correct - the uHAT should sit perfectly above the Pi with 
all four mounting holes aligned with the Pi's.
4. Plug in a micro-USB cable to the uHAT's micro-USB power port.
**Note:** At this point, when a power brick/bank is plugged in to the micro-USB cable, the Pi can be used as normal.
5. Plug a single cell Li-Ion battery into the Pi's JST-B2B-PH-K connector.
6. If the battery is charged, or a power source is connected to the micro-USB cable the Pi should power on.

At this point, the Pi and uHAT should be fully operational.
