<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
    "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">

<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
</head>

<body>
    <h1>Juicer UPS uHAT Bill of Materials</h1>
    <p><b>Component Count:</b>64</p>
    <table>
        <tr>
            <th>Qnty</th>
            <th>Value</th>
            <th>Part</th>
            <th>Description</th>
        </tr>
        <tr>
            <td>1</td>
            <td>4.2V Li-Ion</td>
            <td>Device:Battery_Cell</td>
            <td>Single-cell battery</td>
        </tr>
        <tr>
            <td>2</td>
            <td>1uF</td>
            <td>Device:C</td>
            <td>Unpolarized capacitor</td>
        </tr>
        <tr>
            <td>1</td>
            <td>1n</td>
            <td>Device:C</td>
            <td>Unpolarized capacitor</td>
        </tr>
        <tr>
            <td>1</td>
            <td>10u</td>
            <td>Device:C</td>
            <td>Unpolarized capacitor</td>
        </tr>
        <tr>
            <td>1</td>
            <td>4.7u</td>
            <td>Device:C</td>
            <td>Unpolarized capacitor</td>
        </tr>
        <tr>
            <td>1</td>
            <td>150p</td>
            <td>Device:C</td>
            <td>Unpolarized capacitor</td>
        </tr>
        <tr>
            <td>1</td>
            <td>1.5n</td>
            <td>Device:C</td>
            <td>Unpolarized capacitor</td>
        </tr>
        <tr>
            <td>1</td>
            <td>100n</td>
            <td>Device:C</td>
            <td>Unpolarized capacitor</td>
        </tr>
        <tr>
            <td>2</td>
            <td>22u</td>
            <td>Device:C</td>
            <td>Unpolarized capacitor</td>
        </tr>
        <tr>
            <td>1</td>
            <td>2.2m</td>
            <td>Device:C</td>
            <td>Unpolarized capacitor</td>
        </tr>
        <tr>
            <td>1</td>
            <td>3V3</td>
            <td>Device:D_Zener</td>
            <td>Zener diode</td>
        </tr>
        <tr>
            <td>1</td>
            <td>TLHG4205</td>
            <td>Device:LED</td>
            <td>Light emitting diode</td>
        </tr>
        <tr>
            <td>1</td>
            <td>TLHY4205</td>
            <td>Device:LED</td>
            <td>Light emitting diode</td>
        </tr>
        <tr>
            <td>1</td>
            <td>TLHR4205</td>
            <td>Device:LED</td>
            <td>Light emitting diode</td>
        </tr>
        <tr>
            <td>2</td>
            <td>VLMW41S1T1-5K8L-08</td>
            <td>Device:LED</td>
            <td>Lght emitting diode</td>
        </tr>
        <tr>
            <td>1</td>
            <td>ADA4505-4ARUZ</td>
            <td>ADA4505-4ARUZ:ADA4505-4ARUZ</td>
            <td>Analog Devices ADA4505-4ARUZ, Quad Low Power Op Amp, 50kHz Rail-Rail, 1.8 5 V, 14-Pin TSSOP</td>
        </tr>
        <tr>
            <td>1</td>
            <td>LTC3124IFE#PBF</td>
            <td>LTC3124IFE#PBF:LTC3124IFE#PBF</td>
            <td>LINEAR TECHNOLOGY - LTC3124IFE#PBF - SYNCHRONOUS STEP-UP DC/DC CONVERTER</td>
        </tr>
        <tr>
            <td>1</td>
            <td>USB_B_Micro</td>
            <td>Connector:USB_B_Micro</td>
            <td>USB Micro Type B connector</td>
        </tr>
        <tr>
            <td>1</td>
            <td>GPIO_CONNECTOR</td>
            <td>Connector_Generic:Conn_02x20_Odd_Even</td>
            <td>Generic connector, double row, 02x20, odd/even pin numbering scheme (row 1 odd numbers, row 2 even numbers), script generated (kicad-library-utils/schlib/autogen/connector/)</td>
        </tr>
        <tr>
            <td>2</td>
            <td>3.3u</td>
            <td>Device:Inductor</td>
            <td>Inductor</td>
        </tr>
        <tr>
            <td>2</td>
            <td>Q_PMOS_DGS</td>
            <td>Device:Q_PMOS_DGS</td>
            <td>P-MOSFET transistor, drain/gate/source</td>
        </tr>
        <tr>
            <td>1</td>
            <td>Q_NMOS_DSG</td>
            <td>Device:Q_NMOS_DSG</td>
            <td>N-MOSFET transistor, drain/source/gate</td>
        </tr>
        <tr>
            <td>1</td>
            <td>Q_NMOS_DGS</td>
            <td>Device:Q_NMOS_DGS</td>
            <td>N-MOSFET transistor, drain/gate/source</td>
        </tr>
        <tr>
            <td>1</td>
            <td>10k</td>
            <td>Device:Resistor</td>
            <td>Resistor</td>
        </tr>
        <tr>
            <td>1</td>
            <td>9k</td>
            <td>Device:Resistor</td>
            <td>Resistor</td>
        </tr>
        <tr>
            <td>2</td>
            <td>1k</td>
            <td>Device:Resistor</td>
            <td>Resistor</td>
        </tr>
        <tr>
            <td>1</td>
            <td>60k</td>
            <td>Device:Resistor</td>
            <td>Resistor</td>
        </tr>
        <tr>
            <td>1</td>
            <td>5k6</td>
            <td>Device:Resistor</td>
            <td>Resistor</td>
        </tr>
        <tr>
            <td>1</td>
            <td>240</td>
            <td>Device:Resistor</td>
            <td>Resistor</td>
        </tr>
        <tr>
            <td>2</td>
            <td>820</td>
            <td>Device:Resistor</td>
            <td>Resistor</td>
        </tr>
        <tr>
            <td>2</td>
            <td>13</td>
            <td>Device:Resistor</td>
            <td>Resistor</td>
        </tr>
        <tr>
            <td>1</td>
            <td>13k</td>
            <td>Device:Resistor</td>
            <td>Resistor</td>
        </tr>
        <tr>
            <td>1</td>
            <td>330</td>
            <td>Device:Resistor</td>
            <td>Resistor</td>
        </tr>
        <tr>
            <td>2</td>
            <td>130</td>
            <td>Device:Resistor</td>
            <td>Resistor</td>
        </tr>
        <tr>
            <td>1</td>
            <td>150</td>
            <td>Device:Resistor</td>
            <td>Resistor</td>
        </tr>
        <tr>
            <td>2</td>
            <td>85</td>
            <td>Device:Resistor</td>
            <td>Resistor</td>
        </tr>
        <tr>
            <td>2</td>
            <td>2k</td>
            <td>Device:Resistor</td>
            <td>Resistor</td>
        </tr>
        <tr>
            <td>1</td>
            <td>100k</td>
            <td>Device:Resistor</td>
            <td>Resistor</td>
        </tr>
        <tr>
            <td>1</td>
            <td>1.24k</td>
            <td>Device:Resistor</td>
            <td>Resistor</td>
        </tr>
        <tr>
            <td>1</td>
            <td>28.7k</td>
            <td>Device:Resistor</td>
            <td>Resistor</td>
        </tr>
        <tr>
            <td>1</td>
            <td>990k</td>
            <td>Device:Resistor</td>
            <td>Resistor</td>
        </tr>
        <tr>
            <td>1</td>
            <td>1M</td>
            <td>Device:Resistor</td>
            <td>Resistor</td>
        </tr>
        <tr>
            <td>1</td>
            <td>20m</td>
            <td>Device:Resistor</td>
            <td>Resistor</td>
        </tr>
        <tr>
            <td>1</td>
            <td>31.6k</td>
            <td>Device:Resistor</td>
            <td>Resistor</td>
        </tr>
        <tr>
            <td>1</td>
            <td>1.47M</td>
            <td>Device:Resistor</td>
            <td>Resistor</td>
        </tr>
        <tr>
            <td>1</td>
            <td>464k</td>
            <td>Device:Resistor</td>
            <td>Resistor</td>
        </tr>
        <tr>
            <td>1</td>
            <td>AD820BRZ-REEL-REEL7</td>
            <td>AD820BRZ-REEL-REEL7</td>
            <td>Precision, low power FET input op amp</td>
        </tr>
        <tr>
            <td>1</td>
            <td>LTC4412xS6</td>
            <td>LTC4412xS6</td>
            <td>Low Loss PowerPath Controller, TSOT-23-6</td>
        </tr>
        <tr>
            <td>1</td>
            <td>LTC4077</td>
            <td>LTC4077</td>
            <td></td>
        </tr>
        <!--TABLEROW-->
    </table>
</body>

</html>
