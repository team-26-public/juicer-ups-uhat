# Manufacturing the Juicer UPS uHAT
*Note: this how-to assumes that the latest version of KiCad is installed.*

To produce the Gerber files required to manufacture this board:
1. Open the Juicer UPS uHAT PCB in KiCads' PCB view;
2. Click File -> Plot -> Gerber Files;
3. Ensure that the necessary layers are selected (usually: F.Cu, B.Cu, F.Paste, B.Paste, F.SilkS, B.SilkS, F.Mask, B.Mask, Edge Cuts);
4. Select your desired output directory;
5. Click Plot;
6. Once the Gerber files have been generated, click "Generate Drill Files..."

The Gerber and drill files can then be uploaded to your PCB manufacturer of choice's website for fabrication.
