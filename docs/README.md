# Documentation

## PCB
- [Operation](docs/pcb/Operation.md)
- [Bill of Materials](docs/pcb/BOM.md)
- [Manufacturing](docs/pcb/Manufacturing.md)

## PSU & Battery Charging Submodule
- [Reference Manual](docs/psu/reference_manual.md)

## Gas Gauge & Status LEDs Submodule
- [Interpreting the gas gauge and status LEDs](docs/gas_gauge_status_leds/LED_Interpretation.md)

## Analogue Battery Status Submodule
- [How to Read the Battery Voltage Signal via GPIO](docs/analogue_status_signal/How_To_Read_Analogue_Signal_with_Pi.md)
