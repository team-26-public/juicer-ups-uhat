<style type="text/css">
    table.tableizer-table {
    		font-size: 12px;
    		border: 1px solid #CCC; 
    		font-family: Arial, Helvetica, sans-serif;
    	} 
    	.tableizer-table td {
    		padding: 4px;
    		margin: 3px;
    		border: 1px solid #CCC;
    	}
    	.tableizer-table th {
    		background-color: #104E8B; 
    		color: #FFF;
    		font-weight: bold;
    	}
</style>
<table class="tableizer-table">
    <thead>
        <tr class="tableizer-firstrow">
            <th>No.</th>
            <th>Part Description</th>
            <th>Use</th>
            <th>Datasheet</th>
            <th>Notes</th>
        </tr>
    </thead>
    <tbody>
        <tr>
            <td>1</td>
            <td>micro-USB Female Connector</td>
            <td>Power input for the Pi and the uHAT via micro-USB.</td>
            <td>&nbsp;</td>
            <td>Full component not shown. Port must be soldered on manually.</td>
        </tr>
        <tr>
            <td>2</td>
            <td>LTC4077 Battery Charger</td>
            <td>Controls the charging of the Li-Ion battery.</td>
            <td>https://www.analog.com/media/en/technical-documentation/data-sheets/4077fa.pdf</td>
            <td>&nbsp;</td>
        </tr>
        <tr>
            <td>3</td>
            <td>JST-B2B-PH-K</td>
            <td>Connector for external battery.</td>
            <td>&nbsp;</td>
            <td>Ensure that a single-cell Li-Ion battery is used. If a non-conventional connector is used note that the left pin is positive and the right one is negative.</td>
        </tr>
        <tr>
            <td>&nbsp;</td>
            <td>Battery Socket</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
        </tr>
        <tr>
            <td>4</td>
            <td>LTC4412 Power selector</td>
            <td>Controls whether the Pi and uHAT receive regulated power from the micro-USB input or the battery.</td>
            <td>https://www.analog.com/media/en/technical-documentation/data-sheets/4412fb.pdf</td>
            <td>&nbsp;</td>
        </tr>
        <tr>
            <td>5</td>
            <td>VLMW41S1T1-8K8L-08</td>
            <td>This LED indicates if the uHAT is receiving power via its micro-USB input. If it is receiving power, the LED will turn on. If not, the LED will remain off.</td>
            <td>https://www.vishay.com/docs/81370/vlmw41.pdf</td>
            <td>&nbsp;</td>
        </tr>
        <tr>
            <td>&nbsp;</td>
            <td>White LED (D5 on PCB)</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
        </tr>
        <tr>
            <td>6</td>
            <td>VLMW41S1T1-8K8L-08</td>
            <td>The LED indicates whether the uHAT is charging the attached battery or not. The LED will be off if the battery is fully charged, or if no micro-USB input is detected. The LED will be on when a micro-USB input is present and there is an attached battery which is not fully charged.</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
        </tr>
        <tr>
            <td>&nbsp;</td>
            <td>White LED (D6 on PCB)</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
        </tr>
        <tr>
            <td>7</td>
            <td> TLHY4205</td>
            <td>These LEDS represent the SOC of the attached battery.</td>
            <td>https://www.vishay.com/docs/83005/tlhg420.pdf</td>
            <td>&nbsp;</td>
        </tr>
        <tr>
            <td>&nbsp;</td>
            <td>Yellow LED (D3 on PCB)</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
        </tr>
        <tr>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>The SOC for a given battery voltage may vary. The turn-off thresholds are:</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
        </tr>
        <tr>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>? D2 (Green): Vbat = 3.6V</td>
            <td>&nbsp;</td>
            <td>&nbp;</td>
        </tr>
        <tr>
            <td>8</td>
            <td>TLHG4205Green LED (D2 on PCB)</td>
            <td>? D3 (Yellow): Vbat = 3.3V</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
        </tr>
        <tr>
            <td>9</td>
            <td>TLHR4205</td>
            <td>? D4 (Red): Vbat = 3.1V</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
        </tr>
        <tr>
            <td>&nbsp;</td>
            <td>Red LED (D2 on PCB)</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
        </tr>
        <tr>
            <td>10</td>
            <td>LTC3124 Buck/Boost Converter</td>
            <td>Boosts and regulates the battery voltage to output 5V for the Pi and uHAT circuitry.</td>
            <td>https://eu.mouser.com/datasheet/2/609/3124f-1271619.pdf 0</td>
            <td>&nbsp;</td>
        </tr>
        <tr>
            <td>11</td>
            <td>ADA4505-4 Quad Opamp</td>
            <td>Opamp for amplification and comparator purposes.</td>
            <td>https://www.analog.com/media/en/technical-documentation/data-sheets/ADA4505-1_4505-2_4505-4.pdf</td>
            <td>&nbsp;</td>
        </tr>
        <tr>
            <td>12</td>
            <td>AD820BRZ Single Opamp</td>
            <td>Opamp buffer for feeding battery level signal to Pi GPIO.</td>
            <td>https://www.analog.com/media/en/technical-documentation/data-sheets/AD820.pdf</td>
            <td></td>
        </tr>
    </tbody>
</table>
