# How to Read Analogue Signals with the Raspberry Pi Zero/Zero W

Since the Raspberry Pi Zero does not have an onboard ADC, an external chip must be used.

A concise guide on how to connect the MCP3008 ADC is available [here](https://learn.adafruit.com/reading-a-analog-in-and-controlling-audio-volume-with-the-raspberry-pi?view=all).

Note: the input to the ADC in the case of the Juicer UPS uHAT is from PIN 17.
