# How to Interpret the Juicer UPS uHAT's Gas Gauge and Status LEDs

There are five LEDs on the Juicer UPS uHAT. 
Three of the LEDS are coloured (green, yellow, red). These belong to the the uHAT's gas gauge circuit.
The remaining two LEDs are white, and the HAT's power status.

## Interpreting the Gas Gauge
The three LEDs (green, yellow, red) represent the state of charge (SOC) of the battery.

As the battery discharges, the LEDs will turn off sequentially in the order listed above.
The turn-off thresholds for each of the LEDs is listed below:

| PCB Component Number | Color  | Turn-off Threshold |
|----------------------|--------|--------------------|
| D2                   | Green  | 3.6V               |
| D3                   | Yellow | 3.3V               |
| D4                   | Red    | 3.1V               |

## Interpreting the Battery Status LEDs
The two white LEDs on the Juicer UPS uHAT relate to the power status of the HAT.
The interpretations of the LEDs are below:

| PCB Component Number | Meaning                                                                                                                              |
|----------------------|--------------------------------------------------------------------------------------------------------------------------------------|
| D5                   | - ON: uHAT is receiving power via micro-USB <br> - OFF: uHAT is not receiving power via micro-USB                                   |
| D6                   | - ON: micro-USB power is present and is being used to charge the battery <br> - OFF: The battery is not being charged via micro-USB |
