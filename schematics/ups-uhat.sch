EESchema Schematic File Version 4
EELAYER 30 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 1 4
Title "Raspberry Pi Zero (W) uHAT Template Board"
Date "2019-02-28"
Rev "1.0"
Comp ""
Comment1 "This Schematic is licensed under MIT Open Source License."
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L Connector_Generic:Conn_02x20_Odd_Even J2
U 1 1 5C77771F
P 7450 4750
F 0 "J2" H 7500 5867 50  0000 C CNN
F 1 "GPIO_CONNECTOR" H 7500 5776 50  0000 C CNN
F 2 "lib:PinSocket_2x20_P2.54mm_Vertical_Centered_Anchor" H 7450 4750 50  0001 C CNN
F 3 "~" H 7450 4750 50  0001 C CNN
	1    7450 4750
	1    0    0    -1  
$EndComp
Wire Wire Line
	7250 4250 7050 4250
Wire Wire Line
	7050 4250 7050 5050
Wire Wire Line
	7250 5050 7050 5050
Connection ~ 7050 5050
Wire Wire Line
	7050 5050 7050 5750
Wire Wire Line
	7250 5750 7050 5750
Connection ~ 7050 5750
Wire Wire Line
	7050 5750 7050 5900
Wire Wire Line
	7750 4050 7950 4050
Wire Wire Line
	7950 4050 7950 4450
Wire Wire Line
	7750 4450 7950 4450
Connection ~ 7950 4450
Wire Wire Line
	7950 4450 7950 4750
Wire Wire Line
	7750 4750 7950 4750
Connection ~ 7950 4750
Wire Wire Line
	7750 5250 7950 5250
Wire Wire Line
	7950 4750 7950 5250
Connection ~ 7950 5250
Wire Wire Line
	7950 5250 7950 5450
Wire Wire Line
	7750 5450 7950 5450
Connection ~ 7950 5450
Wire Wire Line
	7950 5450 7950 5900
Wire Wire Line
	7750 3850 8050 3850
Wire Wire Line
	7750 3950 8050 3950
Wire Wire Line
	8050 3950 8050 3850
Connection ~ 8050 3850
Text Notes 8600 3950 0    50   ~ 10
If back powering Pi with 5V \nNOTE that the Raspberry Pi 3B+ and Pi Zero \nand ZeroW do not include an input ZVD.
Wire Notes Line
	8550 3650 8550 4000
Wire Notes Line
	8550 4000 10400 4000
Wire Notes Line
	10400 4000 10400 3650
Wire Notes Line
	10400 3650 8550 3650
$Comp
L power:PWR_FLAG #FLG01
U 1 1 5C77CEFA
P 8400 3750
F 0 "#FLG01" H 8400 3825 50  0001 C CNN
F 1 "PWR_FLAG" H 8400 3924 50  0000 C CNN
F 2 "" H 8400 3750 50  0001 C CNN
F 3 "~" H 8400 3750 50  0001 C CNN
	1    8400 3750
	1    0    0    -1  
$EndComp
Wire Wire Line
	8050 3850 8400 3850
Wire Wire Line
	8400 3750 8400 3850
Text Label 6300 3950 0    50   ~ 0
GPIO2_SDA1
Text Label 6300 4050 0    50   ~ 0
GPIO3_SCL1
Text Label 6300 4150 0    50   ~ 0
GPIO4_GPIO_GCLK
Text Label 6300 4350 0    50   ~ 0
GPIO17_GEN0
Text Label 6300 4450 0    50   ~ 0
GPIO27_GEN2
Text Label 6300 4550 0    50   ~ 0
GPIO22_GEN3
Text Label 6300 4750 0    50   ~ 0
GPIO10_SPI_MOSI
Wire Wire Line
	6200 4750 7250 4750
Wire Wire Line
	6200 4850 7250 4850
Wire Wire Line
	6200 4950 7250 4950
Wire Wire Line
	6200 5150 7250 5150
Wire Wire Line
	6200 5250 7250 5250
Wire Wire Line
	6200 5350 7250 5350
Wire Wire Line
	6200 5450 7250 5450
Wire Wire Line
	6200 5550 7250 5550
Wire Wire Line
	6200 5650 7250 5650
Wire Wire Line
	6200 4550 7250 4550
Wire Wire Line
	6200 4450 7250 4450
Wire Wire Line
	6200 4150 7250 4150
Wire Wire Line
	6200 4050 7250 4050
Wire Wire Line
	6200 3950 7250 3950
Text Label 6300 4850 0    50   ~ 0
GPIO9_SPI_MISO
Text Label 6300 4950 0    50   ~ 0
GPIO11_SPI_SCLK
Text Label 6300 5150 0    50   ~ 0
ID_SD
Text Label 6300 5250 0    50   ~ 0
GPIO5
Text Label 6300 5350 0    50   ~ 0
GPIO6
Text Label 6300 5450 0    50   ~ 0
GPIO13
Text Label 6300 5550 0    50   ~ 0
GPIO19
Text Label 6300 5650 0    50   ~ 0
GPIO26
NoConn ~ 6200 3950
NoConn ~ 6200 4050
NoConn ~ 6200 4150
NoConn ~ 6200 4450
NoConn ~ 6200 4550
NoConn ~ 6200 4750
NoConn ~ 6200 4850
NoConn ~ 6200 4950
NoConn ~ 6200 5150
NoConn ~ 6200 5250
NoConn ~ 6200 5350
NoConn ~ 6200 5450
NoConn ~ 6200 5550
NoConn ~ 6200 5650
Text Label 8100 4150 0    50   ~ 0
GPIO14_TXD0
Text Label 8100 4250 0    50   ~ 0
GPIO15_RXD0
Text Label 8100 4350 0    50   ~ 0
GPIO18_GEN1
Text Label 8100 4550 0    50   ~ 0
GPIO23_GEN4
Text Label 8100 4650 0    50   ~ 0
GPIO24_GEN5
Text Label 8100 4850 0    50   ~ 0
GPIO25_GEN6
Text Label 8100 4950 0    50   ~ 0
GPIO8_SPI_CE0_N
Text Label 8100 5050 0    50   ~ 0
GPIO7_SPI_CE1_N
Wire Wire Line
	7750 4950 8800 4950
Wire Wire Line
	7750 5050 8800 5050
Text Label 8100 5150 0    50   ~ 0
ID_SC
Text Label 8100 5350 0    50   ~ 0
GPIO12
Text Label 8100 5550 0    50   ~ 0
GPIO16
Text Label 8100 5650 0    50   ~ 0
GPIO20
Text Label 8100 5750 0    50   ~ 0
GPIO21
Wire Wire Line
	7750 4150 8800 4150
Wire Wire Line
	7750 4250 8800 4250
Wire Wire Line
	7750 4350 8800 4350
Wire Wire Line
	7750 4550 8800 4550
Wire Wire Line
	7750 4650 8800 4650
Wire Wire Line
	7750 4850 8800 4850
Wire Wire Line
	7750 5150 8800 5150
Wire Wire Line
	7750 5350 8800 5350
Wire Wire Line
	7750 5550 8800 5550
Wire Wire Line
	7750 5650 8800 5650
NoConn ~ 8800 4150
NoConn ~ 8800 4250
NoConn ~ 8800 4350
NoConn ~ 8800 4550
NoConn ~ 8800 4650
NoConn ~ 8800 4850
NoConn ~ 8800 4950
NoConn ~ 8800 5050
NoConn ~ 8800 5150
NoConn ~ 8800 5350
NoConn ~ 8800 5550
NoConn ~ 8800 5650
NoConn ~ 8800 5750
Wire Wire Line
	7750 5750 8800 5750
$Comp
L Mechanical:MountingHole H1
U 1 1 5C7C4C81
P 10450 4400
F 0 "H1" H 10550 4446 50  0000 L CNN
F 1 "MountingHole" H 10550 4355 50  0000 L CNN
F 2 "lib:MountingHole_2.7mm_M2.5_uHAT_RPi" H 10450 4400 50  0001 C CNN
F 3 "~" H 10450 4400 50  0001 C CNN
	1    10450 4400
	1    0    0    -1  
$EndComp
$Comp
L Mechanical:MountingHole H2
U 1 1 5C7C7FBC
P 10450 4600
F 0 "H2" H 10550 4646 50  0000 L CNN
F 1 "MountingHole" H 10550 4555 50  0000 L CNN
F 2 "lib:MountingHole_2.7mm_M2.5_uHAT_RPi" H 10450 4600 50  0001 C CNN
F 3 "~" H 10450 4600 50  0001 C CNN
	1    10450 4600
	1    0    0    -1  
$EndComp
$Comp
L Mechanical:MountingHole H3
U 1 1 5C7C8014
P 10450 4800
F 0 "H3" H 10550 4846 50  0000 L CNN
F 1 "MountingHole" H 10550 4755 50  0000 L CNN
F 2 "lib:MountingHole_2.7mm_M2.5_uHAT_RPi" H 10450 4800 50  0001 C CNN
F 3 "~" H 10450 4800 50  0001 C CNN
	1    10450 4800
	1    0    0    -1  
$EndComp
$Comp
L Mechanical:MountingHole H4
U 1 1 5C7C8030
P 10450 5000
F 0 "H4" H 10550 5046 50  0000 L CNN
F 1 "MountingHole" H 10550 4955 50  0000 L CNN
F 2 "lib:MountingHole_2.7mm_M2.5_uHAT_RPi" H 10450 5000 50  0001 C CNN
F 3 "~" H 10450 5000 50  0001 C CNN
	1    10450 5000
	1    0    0    -1  
$EndComp
$Sheet
S 5050 1500 2050 900 
U 60B7B31A
F0 "Analogue Battery Status Sub-Module" 50
F1 "Analogue Battery Status/Analogue Battery Status.sch" 50
F2 "5V" I L 5050 1650 50 
F3 "Vbat" I L 5050 2050 50 
F4 "Vcharge" I R 7100 1650 50 
$EndSheet
$Sheet
S 1150 4600 2150 1250
U 60B7B551
F0 "Gas Gauge & Status LED Sub-Module" 50
F1 "Gas Gauge Status LEDs/Gas Gauge Status LEDs.sch" 50
F2 "5V" I R 3300 4850 50 
F3 "~PWR" I R 3300 5700 50 
F4 "~CHRG" I R 3300 5350 50 
F5 "Vbat" I R 3300 5100 50 
$EndSheet
$Sheet
S 1150 1700 2300 1300
U 60B79A90
F0 "Battery Charger & PSU Sub-Module" 50
F1 "Battery Charger and PSU/Battery Charger and PSU.sch" 50
F2 "5Vwall" I L 1150 2000 50 
F3 "5V" I R 3450 2050 50 
F4 "~PWR" I R 3450 2750 50 
F5 "~CHRG" I R 3450 2550 50 
F6 "Vbat" I R 3450 2350 50 
F7 "GND'" I R 3450 2950 50 
$EndSheet
Wire Wire Line
	3450 2050 3800 2050
Wire Wire Line
	4450 2050 4450 1650
Wire Wire Line
	4450 1650 4800 1650
Wire Wire Line
	5050 2050 4650 2050
Wire Wire Line
	4650 2050 4650 2350
Wire Wire Line
	4650 2350 4050 2350
Wire Wire Line
	3800 2050 3800 4850
Wire Wire Line
	3800 4850 3300 4850
Connection ~ 3800 2050
Wire Wire Line
	3800 2050 4450 2050
Wire Wire Line
	4050 2350 4050 5100
Wire Wire Line
	4050 5100 3300 5100
Connection ~ 4050 2350
Wire Wire Line
	4050 2350 3450 2350
Wire Wire Line
	3300 5700 3600 5700
Wire Wire Line
	3600 5700 3600 2750
Wire Wire Line
	3600 2750 3450 2750
Wire Wire Line
	3450 2550 4300 2550
Wire Wire Line
	4300 2550 4300 5350
Wire Wire Line
	4300 5350 3300 5350
Wire Wire Line
	7950 5900 7050 5900
Wire Wire Line
	7050 5900 7050 6250
Wire Wire Line
	7050 6250 4800 6250
Wire Wire Line
	4800 6250 4800 2950
Wire Wire Line
	4800 2950 3450 2950
Connection ~ 7050 5900
Wire Wire Line
	7100 1650 7600 1650
Wire Wire Line
	7600 1650 7600 3200
Wire Wire Line
	7600 3200 6000 3200
Wire Wire Line
	6000 3200 6000 4350
Wire Wire Line
	6000 4350 7250 4350
Wire Wire Line
	4800 1650 4800 1400
Wire Wire Line
	4800 1400 8050 1400
Wire Wire Line
	8050 1400 8050 3850
Connection ~ 4800 1650
Wire Wire Line
	4800 1650 5050 1650
$Comp
L Connector:USB_B_Micro J1
U 1 1 60CE1054
P 750 2200
F 0 "J1" H 807 2667 50  0000 C CNN
F 1 "USB_B_Micro" H 807 2576 50  0000 C CNN
F 2 "Connector_USB:USB_Micro-B_Molex-105133-0031" H 900 2150 50  0001 C CNN
F 3 "~" H 900 2150 50  0001 C CNN
	1    750  2200
	1    0    0    -1  
$EndComp
Wire Wire Line
	1150 2000 1050 2000
NoConn ~ 1050 2200
NoConn ~ 1050 2300
NoConn ~ 1050 2400
NoConn ~ 650  2600
$Comp
L power:GND #PWR01
U 1 1 60CF96CC
P 750 2600
F 0 "#PWR01" H 750 2350 50  0001 C CNN
F 1 "GND" H 755 2427 50  0000 C CNN
F 2 "" H 750 2600 50  0001 C CNN
F 3 "" H 750 2600 50  0001 C CNN
	1    750  2600
	1    0    0    -1  
$EndComp
NoConn ~ 7250 4650
NoConn ~ 7250 3850
$EndSCHEMATC
